import { actionTree, getterTree, mutationTree } from 'typed-vuex';

export const state = () => ({
  trading: 'https://dex.keramos.tech',
  explorer: 'https://solscan.io',
  trade: 'https://dex.keramos.tech',
  dropZone: 'https://dex.keramos.tech',
  'browse-NFTs': 'https://dex.keramos.tech',
  'explore-Collections': 'https://dex.keramos.tech'
})

export const getters = getterTree(state, {})

export const mutations = mutationTree(state, {})

export const actions = actionTree({ state, getters, mutations }, {})
