import dayjs from 'dayjs'

import 'dayjs/locale/en'
import utc from 'dayjs/plugin/utc'

dayjs.extend(utc)

dayjs.locale('en')

export default (context, inject) => {
  context.$dayjs = dayjs
  inject('dayjs', dayjs)
}
