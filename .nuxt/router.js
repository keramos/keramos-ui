import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _3b7b9260 = () => interopDefault(import('../src/pages/acceleraytor/index.vue' /* webpackChunkName: "pages/acceleraytor/index" */))
const _31f73920 = () => interopDefault(import('../src/pages/debug.vue' /* webpackChunkName: "pages/debug" */))
const _9d22d1cc = () => interopDefault(import('../src/pages/farms.vue' /* webpackChunkName: "pages/farms" */))
const _5af46236 = () => interopDefault(import('../src/pages/index copy.vue' /* webpackChunkName: "pages/index copy" */))
const _4edd00be = () => interopDefault(import('../src/pages/info.vue' /* webpackChunkName: "pages/info" */))
const _400eb052 = () => interopDefault(import('../src/pages/liquidity/index.vue' /* webpackChunkName: "pages/liquidity/index" */))
const _2b5a4988 = () => interopDefault(import('../src/pages/migrate.vue' /* webpackChunkName: "pages/migrate" */))
const _ebf85218 = () => interopDefault(import('../src/pages/pools.vue' /* webpackChunkName: "pages/pools" */))
const _742c7794 = () => interopDefault(import('../src/pages/staking.vue' /* webpackChunkName: "pages/staking" */))
const _8a099bf4 = () => interopDefault(import('../src/pages/swap.vue' /* webpackChunkName: "pages/swap" */))
const _b249b9c2 = () => interopDefault(import('../src/pages/verifytg.vue' /* webpackChunkName: "pages/verifytg" */))
const _564ae249 = () => interopDefault(import('../src/pages/winner-list.vue' /* webpackChunkName: "pages/winner-list" */))
const _18b6b9d3 = () => interopDefault(import('../src/pages/activities/airdrop-binance.vue' /* webpackChunkName: "pages/activities/airdrop-binance" */))
const _997fad3c = () => interopDefault(import('../src/pages/activities/airdrop-okex.vue' /* webpackChunkName: "pages/activities/airdrop-okex" */))
const _323c2a7d = () => interopDefault(import('../src/pages/activities/winner-list.vue' /* webpackChunkName: "pages/activities/winner-list" */))
const _7a641ecc = () => interopDefault(import('../src/pages/docs/disclaimer.vue' /* webpackChunkName: "pages/docs/disclaimer" */))
const _bc56cb66 = () => interopDefault(import('../src/pages/liquidity/create-pool.vue' /* webpackChunkName: "pages/liquidity/create-pool" */))
const _78eaa670 = () => interopDefault(import('../src/pages/acceleraytor/_id.vue' /* webpackChunkName: "pages/acceleraytor/_id" */))
const _7a98c322 = () => interopDefault(import('../src/pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/acceleraytor",
    component: _3b7b9260,
    name: "acceleraytor"
  }, {
    path: "/debug",
    component: _31f73920,
    name: "debug"
  }, {
    path: "/farms",
    component: _9d22d1cc,
    name: "farms"
  }, {
    path: "/index%20copy",
    component: _5af46236,
    name: "index copy"
  }, {
    path: "/info",
    component: _4edd00be,
    name: "info"
  }, {
    path: "/liquidity",
    component: _400eb052,
    name: "liquidity"
  }, {
    path: "/migrate",
    component: _2b5a4988,
    name: "migrate"
  }, {
    path: "/pools",
    component: _ebf85218,
    name: "pools"
  }, {
    path: "/staking",
    component: _742c7794,
    name: "staking"
  }, {
    path: "/swap",
    component: _8a099bf4,
    name: "swap"
  }, {
    path: "/verifytg",
    component: _b249b9c2,
    name: "verifytg"
  }, {
    path: "/winner-list",
    component: _564ae249,
    name: "winner-list"
  }, {
    path: "/activities/airdrop-binance",
    component: _18b6b9d3,
    name: "activities-airdrop-binance"
  }, {
    path: "/activities/airdrop-okex",
    component: _997fad3c,
    name: "activities-airdrop-okex"
  }, {
    path: "/activities/winner-list",
    component: _323c2a7d,
    name: "activities-winner-list"
  }, {
    path: "/docs/disclaimer",
    component: _7a641ecc,
    name: "docs-disclaimer"
  }, {
    path: "/liquidity/create-pool",
    component: _bc56cb66,
    name: "liquidity-create-pool"
  }, {
    path: "/acceleraytor/:id",
    component: _78eaa670,
    name: "acceleraytor-id"
  }, {
    path: "/",
    component: _7a98c322,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
