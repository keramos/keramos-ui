import { getAccessorFromStore } from 'typed-vuex'

import { createStore } from '/Users/vchittchang/_2021/Github/keramos-ui/.nuxt/store'

const storeAccessor = getAccessorFromStore(createStore())

export default async ({ store }, inject) => {
  inject('accessor', storeAccessor(store))
}
