# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<AmmIdSelect>` | `<amm-id-select>` (components/AmmIdSelect.vue)
- `<CoinIcon>` | `<coin-icon>` (components/CoinIcon.vue)
- `<CoinInput>` | `<coin-input>` (components/CoinInput.vue)
- `<CoinModal>` | `<coin-modal>` (components/CoinModal.vue)
- `<CoinSelect>` | `<coin-select>` (components/CoinSelect.vue)
- `<CoinSelectSource>` | `<coin-select-source>` (components/CoinSelectSource.vue)
- `<Foot>` | `<foot>` (components/Foot.vue)
- `<HeadChip>` | `<head-chip>` (components/HeadChip.vue)
- `<HeadMain>` | `<head-main>` (components/HeadMain.vue)
- `<InputAmmIdOrMarket>` | `<input-amm-id-or-market>` (components/InputAmmIdOrMarket.vue)
- `<LiquidityPoolInfo>` | `<liquidity-pool-info>` (components/LiquidityPoolInfo.vue)
- `<Nav>` | `<nav>` (components/Nav.vue)
- `<RpcSpeed>` | `<rpc-speed>` (components/RpcSpeed.vue)
- `<Setting>` | `<setting>` (components/Setting.vue)
- `<ToAmountConfrim>` | `<to-amount-confrim>` (components/ToAmountConfrim.vue)
- `<UnofficialCoinConfirmUser>` | `<unofficial-coin-confirm-user>` (components/UnofficialCoinConfirmUser.vue)
- `<UnofficialPoolConfirmUser>` | `<unofficial-pool-confirm-user>` (components/UnofficialPoolConfirmUser.vue)
- `<Wallet>` | `<wallet>` (components/Wallet.vue)
- `<YourLiquidity>` | `<your-liquidity>` (components/YourLiquidity.vue)
