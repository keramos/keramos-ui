export { default as AmmIdSelect } from '../../src/components/AmmIdSelect.vue'
export { default as CoinIcon } from '../../src/components/CoinIcon.vue'
export { default as CoinInput } from '../../src/components/CoinInput.vue'
export { default as CoinModal } from '../../src/components/CoinModal.vue'
export { default as CoinSelect } from '../../src/components/CoinSelect.vue'
export { default as CoinSelectSource } from '../../src/components/CoinSelectSource.vue'
export { default as Foot } from '../../src/components/Foot.vue'
export { default as HeadChip } from '../../src/components/HeadChip.vue'
export { default as HeadMain } from '../../src/components/HeadMain.vue'
export { default as InputAmmIdOrMarket } from '../../src/components/InputAmmIdOrMarket.vue'
export { default as LiquidityPoolInfo } from '../../src/components/LiquidityPoolInfo.vue'
export { default as Nav } from '../../src/components/Nav.vue'
export { default as RpcSpeed } from '../../src/components/RpcSpeed.vue'
export { default as Setting } from '../../src/components/Setting.vue'
export { default as ToAmountConfrim } from '../../src/components/ToAmountConfrim.vue'
export { default as UnofficialCoinConfirmUser } from '../../src/components/UnofficialCoinConfirmUser.vue'
export { default as UnofficialPoolConfirmUser } from '../../src/components/UnofficialPoolConfirmUser.vue'
export { default as Wallet } from '../../src/components/Wallet.vue'
export { default as YourLiquidity } from '../../src/components/YourLiquidity.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
